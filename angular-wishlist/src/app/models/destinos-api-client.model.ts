import { DestinoViaje } from './Destino-Viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';

//redux
import {Injectable} from '@angular/core';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import {
		DestinosViajesState,
		NuevoDestinoAction,
		ElegidoFavoritoAction
	} from './destinos-viajes-state.model';
import {AppState} from './../app.module';

@Injectable()
export class DestinosApiClient {
    //destinos:DestinoViaje[]=[];
    //current:Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    	constructor(private store: Store<AppState>) {
      		/*this.store
      			.select(state => state.destinos)
      			.subscribe((data) => {
      				console.log("destinos sub store");
      				console.log(data);
      				this.destinos = data.items;
      			});
      		this.store
      			.subscribe((data) => {
      				console.log("all store");
      				console.log(data);
      			});*/
      	}

    add(d:DestinoViaje){
      this.store.dispatch(new NuevoDestinoAction(d));
    }
    /*getAll(){
      return this.destinos;
    }

    getById(id:String):DestinoViaje{
	  return this.destinos.filter(function(d){return d.id.toString() == id;})[0];
    }*/
    elegir(d: DestinoViaje){
      this.store.dispatch(new ElegidoFavoritoAction(d));
    }
    }
 